;;; package --- pug mode config
;;; Commentary:
;;; code:


(require 'pug-mode)
(setq indent-tabs-mode nil)
(setq-default tab-width 1)

(defun pug-compile-saved-file()
  (when (and (stringp buffer-file-name)
             (string-match "\\.pug\\'" buffer-file-name))
    (pug-compile)))
(add-hook 'after-save-hook 'pug-compile-saved-file)


(provide 'init-pug)

;;; init-pug ends here
