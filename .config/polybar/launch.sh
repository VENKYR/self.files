#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar



# Wait until the processes have been shut down

# while pgrep -x polybar >/dev/null; do sleep 1; done


# for m in $(polybar --list-monitors | cut -d":" -f1); do
#     MONITOR=$m polybar --reload top;
#     MONITOR=$m polybar --reload bottom;
# done
# for m in $(polybar --list-monitors | cut -d":" -f1); do
#     MONITOR=$m
# done

polybar top
polybar bottom


echo "Bars launched..."
