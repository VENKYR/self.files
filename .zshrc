export TERM="xterm-256color"
autoload -Uz compinit
source /usr/share/zsh-theme-powerlevel9k/powerlevel9k.zsh-theme
# source /usr/lib/python3.7/site-packages/powerline/bindings/zsh/powerline.zsh
export LANG=en_US.UTF-8
export PATH=$PATH:$HOME/bin:/usr/local/bin:$HOME/.gem/ruby/2.5.0/bin:$HOME/.local/bin
export ZSH=$HOME/.oh-my-zsh


ZSH_THEME="robbyrussell"
# ZSH_THEME="ys"
# ZSH_THEME="random"

#ZSH_THEME="powerlevel9k/powerlevel9k"

# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell"  "gnzh" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# DISABLE_AUTO_UPDATE="true"
# export UPDATE_ZSH_DAYS=13
# DISABLE_LS_COLORS="true"
# DISABLE_AUTO_TITLE="true"
ENABLE_CORRECTION="true"
#COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"

# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

plugins=(common-aliases  dotenv archlinux python history chucknorris git
	 last-working-dir pass)

# bind UP and DOWN arrow keys
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down

# bind UP and DOWN arrow keys (compatibility fallback
# for Ubuntu 12.04, Fedora 21, and MacOSX 10.9 users)
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# bind P and N for EMACS mode
bindkey -M emacs '^P' history-substring-search-up
bindkey -M emacs '^N' history-substring-search-down

# bind k and j for VI mode
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

source $ZSH/oh-my-zsh.sh

export ARCHFLAGS="-arch x86_64"
export SSH_KEY_PATH="~/.ssh/rsa_id"


## User configuration

export MANPATH="/usr/local/man:$MANPATH"

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='emacs'
else
    export EDITOR='nano'
fi


# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Py virtual env
export WORKON_HOME=~/.virtualenvs
source /usr/bin/virtualenvwrapper_lazy.sh
# source /bin/aws_zsh_completer.sh
export VIRTUALENVWRAPPER_SCRIPT=/usr/bin/virtualenvwrapper.sh

export VISUAL="nano"

CLASSPATH=/usr/share/R/share/java
LD_LIBRARY_PATH=/usr/lib/jvm/java-8-openjdk/jre/lib/amd64/

## Aliases from bash

#PS1='\[\033[01;32m\]1337@venky\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
# alias cat=$HOME/bin/ccat
alias woman=eg
alias publicip="wget http://ipinfo.io/ip -qO - "
alias ipcity="wget http://ipinfo.io/city -qO - "
alias utube22="youtube-dl --write-srt --sub-lang en --sub-format srt -f 22 -o $HOME/Videos/youtube/%\(title\)s.%\(ext\)s "
alias utube18="youtube-dl --write-srt --sub-lang en --sub-format srt -f 18 -o $HOME/Videos/youtube/%\(title\)s.%\(ext\)s "
alias utubelist="youtube-dl -F "
alias utubepl22="youtube-dl --write-srt --sub-lang en --sub-format srt -f 22 --yes-playlist -o $HOME/Videos/youtube/%\(title\)s.%\(ext\)s "
alias utubepl18="youtube-dl --write-srt --sub-lang en --sub-format srt -f 18 --yes-playlist -o $HOME/Videos/youtube/%\(title\)s.%\(ext\)s "
alias utubemp3="youtube-dl -x --audio-format 'mp3' --audio-quality 0 -o $HOME/Music/youtube-dl/%\(title\)s.%\(ext\)s"
alias utubeplmp3="youtube-dl -x --audio-format 'mp3' --audio-quality 0 --yes-playlist -o $HOME/Music/youtube-dl/%\(title\)s.%\(ext\)s"
alias emptytrash="rm -rf ~/.local/share/Trash/*"
alias x="chmod +x "
alias python3ls="python3 -m http.server"
alias python2ls="python2 -m SimpleHTTPServer "
alias pip3update="pip3 freeze --local | awk -F= '{print $1}' | xargs -n1 sudo -H  pip3 install -U"
alias pip2update="pip2 freeze --local | awk -F= '{print $1}' | xargs -n1 sudo -H  pip3 install -U"
# alias pip3update="pip3 freeze --local | cut -d = -f 1  | xargs -n1 sudo -H  pip3 install -U"
# alias pip2update="pip2 freeze --local | cut -d = -f 1  | xargs -n1 sudo -H  pip3 install -U"
alias pip3install="sudo -H pip3 install -U "
alias pipinstall="sudo -H pip2 install  -U"
alias pip3uninstall="sudo -H pip3 uninstall "
alias pipuninstall="sudo -H pip2 uninstall  "
alias spaces="rename 's/ /_/g' "
alias rmcaps="rename 'y/A-Z/a-z/' "
alias torb="$HOME/tor/Browser/start-tor-browser"
alias please='sudo $(fc -n -l -1)'
alias open='xdg-open'

extract () {
	if [ -f $1 ] ; then
		case $1 in
			*.tar.bz2)   tar xvjf $1    ;;
			*.tar.gz)    tar xvzf $1    ;;
			*.bz2)       bunzip2 $1     ;;
			*.rar)       unrar e $1     ;;
			*.gz)        gunzip $1      ;;
			*.tar)       tar xvf $1     ;;
			*.tbz2)      tar xvjf $1    ;;
			*.tgz)       tar xvzf $1    ;;
			*.zip)       unzip $1       ;;
			*.Z)         uncompress $1  ;;
			*.7z)        7z x $1        ;;
			*)           echo "don't know how to extract "$1"..." ;;
		esac
	else
		echo "'$1' is not a valid file!"
	fi
}

alias start_redis="redis-server --loadmodule $HOME/Documents/GEEK_SK00L/Softwarez/redis_modules/redis_graph/src/redisgraph.so"
# alias start_neo4j="$HOME/Documents/GEEK_SK00L/Backend/DB/Graph_DB/warez/neo4j/neo4j-community-3.3.5/bin/neo4j console &"

# cowfortune
#screenfetch

## Powerlevel9k
POWERLEVEL9K_MODE='awesome-fontconfig'
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
# POWERLEVEL9K_RPROMPT_ON_NEWLINE=true
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
# POWERLEVEL9K_DISABLE_RPROMPT=true

# POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="↱"
# POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="↳ "

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir dir_writable root_indicator virtualenv vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status user background_jobs ssh)
# POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir dir_writable root_indicator newline user node_version rust_version virtualenv vcs)
# POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status disk_usage history command_execution_time background_jobs time host ssh)


POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
# POWERLEVEL9K_SHORTEN_DELIMITER=".."
# POWERLEVEL9K_SHORTEN_STRATEGY="truncate_with_folder_marker"
# POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_right"
# POWERLEVEL9K_STATUS_CROSS=true

POWERLEVEL9K_PYENV_PROMPT_ALWAYS_SHOW=True
POWERLEVEL9K_BACKGROUND_JOBS_VERBOSE_ALWAYS=True
POWERLEVEL9K_DIR_PATH_SEPARATOR="%F{green}  $(print_icon 'LEFT_SUBSEGMENT_SEPARATOR') %F{green}"

POWERLEVEL9K_TIME_FOREGROUND='green'
POWERLEVEL9K_TIME_BACKGROUND='black'
POWERLEVEL9K_TIME_FORMAT="%D{\uf017 %H:%M}"
POWERLEVEL9K_DIR_HOME_FOREGROUND='green'
POWERLEVEL9K_DIR_HOME_BACKGROUND='black'
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND='green'
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND='black'
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND='green'
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND='black'
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND='black'
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_BACKGROUND='magenta'
POWERLEVEL9K_DIR_SHOW_WRITABLE=True
POWERLEVEL9K_DIR_PATH_HIGHLIGHT_BOLD=True

# POWERLEVEL9K_VCS_GIT_ICON=VCS_GIT_GITHUB_ICON
POWERLEVEL9K_VCS_STAGED_ICON='\u00b1'
POWERLEVEL9K_VCS_UNTRACKED_ICON='\u25CF'
POWERLEVEL9K_VCS_UNSTAGED_ICON='\u00b1'
POWERLEVEL9K_VCS_INCOMING_CHANGES_ICON='\u2193'
POWERLEVEL9K_VCS_OUTGOING_CHANGES_ICON='\u2191'
POWERLEVEL9K_VCS_CLEAN_FOREGROUND='green'
POWERLEVEL9K_VCS_CLEAN_BACKGROUND='black'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='black'
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND='blue'
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='red'
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='black'
POWERLEVEL9K_VCS_BRANCH_ICON=$'\uF126 '
# POWERLEVEL9K_HIDE_BRANCH_ICON=True


POWERLEVEL9K_DISK_USAGE_ONLY_WARNING=True
POWERLEVEL9K_HOST_ICON="\uF109 "
POWERLEVEL9K_SSH_ICON="\uF489 "

POWERLEVEL9K_HOME_ICON='\uF015 '
POWERLEVEL9K_HOME_SUB_ICON='\uF015 '
POWERLEVEL9K_FOLDER_ICON='\uF07c '

POWERLEVEL9K_USER_ICON="\uF505 "
POWERLEVEL9K_ROOT_ICON="#"
POWERLEVEL9K_SUDO_ICON=$'\uF09C'

# POWERLEVEL9K_DIR_OMIT_FIRST_CHARACTER=false
POWERLEVEL9K_ALWAYS_SHOW_CONTEXT=true
POWERLEVEL9K_ALWAYS_SHOW_USER=true

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

zstyle ':completion:*' rehash true


export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi
