;;; package --- summary local config
;;; Commentary:
;;; code:


(use-package emms
  :config
  (require 'emms-setup)
  (require 'emms-cache)
  (require 'emms-volume)
  (require 'emms-volume-amixer)
  (require 'emms-player-mpv)
  (require 'emms-source-file)
  (require 'emms-playlist-mode)
  (require 'emms-source-playlist)
  (require 'emms-mode-line)
  (require 'emms-mode-line-icon)
  (emms-all)
  (emms-standard)
  (emms-history-load)
  (setq emms-player-list (list emms-player-mpv)
        emms-source-file-default-directory (expand-file-name "~/Music")
        emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find
        emms-browser-covers 'emms-browser-cache-thumbnail)
  (add-to-list 'emms-player-mpv-parameters "--no-audio-display")
  (add-to-list 'emms-info-functions 'emms-info-cueinfo)
  (if (executable-find "emms-print-metadata")
      (progn
        (require 'emms-info-libtag)
        (add-to-list 'emms-info-functions 'emms-info-libtag)
        (delete 'emms-info-ogginfo emms-info-functions)
        (delete 'emms-info-mp3info emms-info-functions)
        )
    (add-to-list 'emms-info-functions 'emms-info-ogginfo)
    (add-to-list 'emms-info-functions 'emms-info-mp3info)
    )
  (defun z-emms-play-on-add (old-pos)
    "Play tracks when calling `emms-browser-add-tracks' if nothing
                   is currently playing."
    (interactive)
    (when (or (not emms-player-playing-p)
              emms-player-paused-p
              emms-player-stopped-p)
      (with-current-emms-playlist
        (goto-char old-pos)
        ;; if we're sitting on a group name, move forward
        (unless (emms-playlist-track-at (point))
          (emms-playlist-next)
          )
        (emms-playlist-select (point))
        )
      (emms-stop)
      (emms-start))
    )
  (add-hook 'emms-browser-tracks-added-hook 'z-emms-play-on-add)
  ;; Show the current track each time EMMS
  (add-hook 'emms-player-started-hook 'emms-show)
  (setq emms-show-format "Playing: %s")
  ;; Icon setup.
  (setq emms-mode-line-icon-before-format "["
        emms-mode-line-format " %s]"
        emms-playing-time-display-format "%s ]"
        emms-mode-line-icon-color "lightgrey")
  (setq global-mode-string '("" emms-mode-line-string " " emms-playing-time-string))
  (defun emms-mode-line-icon-function ()
    (concat " "
            emms-mode-line-icon-before-format
            (propertize "NP:" 'display emms-mode-line-icon-image-cache)
            (format emms-mode-line-format (emms-track-get
                                           (emms-playlist-current-selected-track)
                                           'info-title))
            )
    )

  (setq emms-directory "~/.emacs.d/etc/emms/"
        emms-source-file-default-directory "~/Music/"
        emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find
        emms-playlist-buffer-name "*Music Playlist*"
        emms-playlist-mode-open-playlists t)
  )



(provide 'init-emms)



;;; init-emms ends here
