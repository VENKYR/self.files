#!/usr/bin/env python3

import dbus

trunclen = 25
session_bus = dbus.SessionBus()


def clean_title(song):
    if song and len(song) > trunclen:
        song = song[0:trunclen] + '...'
        if ('(' in song) and (')' not in song):
            song += ')'
    return song


def print_info():
    try:
        spotify_bus = session_bus.get_object("org.mpris.MediaPlayer2.spotify",
                                             "/org/mpris/MediaPlayer2")
    except dbus.DBusException:
        print("")
        return
    spotify_properties = dbus.Interface(spotify_bus,
                                        "org.freedesktop.DBus.Properties")

    metadata = spotify_properties.Get("org.mpris.MediaPlayer2.Player",
                                      "Metadata")

    if 'xesam:artist' in metadata and 'xesam:title' in metadata:
        artist = metadata['xesam:artist'][0]
        song = clean_title(metadata['xesam:title'])
        output = artist + ': ' + song
        print(output)
    print("")


print_info()
