;;; package --- httpd  config
;;; Commentary:
;;; code:



(setq httpd-root "/var/www")
(httpd-start)

;; (defservlet hello-world text/plain (path)
;;   (insert "hello, " (file-name-nondirectory path)))

(provide 'init-httpd)

;;; init-httpd ends here
