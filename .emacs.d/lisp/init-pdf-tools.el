;;; package --- pdf-tools config
;;; Commentary:
;;; code:



(progn
  (require 'pdf-tools)
  (pdf-tools-install) ;; install pdf-tools
  (add-hook 'pdf-tools-enabled-hook 'auto-revert-mode)

  (define-key pdf-view-mode-map (kbd "C-n") 'pdf-view-next-page-command)
  (define-key pdf-view-mode-map (kbd "C-p") 'pdf-view-previous-page-command)
  (define-key pdf-view-mode-map (kbd "n") 'pdf-view-next-line-or-next-page)
  (define-key pdf-view-mode-map (kbd "p") 'pdf-view-previous-line-or-previous-page)

  (setq pdf-annot-activate-created-annotations t ;; automatically annotate highlights
        ;; more fine-grained zooming
        pdf-view-resize-factor 1.1)
  (add-hook 'pdf-view-mode-hook (lambda () (cua-mode 0)))

  ;; __________________________________________________
  ;; Pdf-tools reopen last page
  ;; https://github.com/politza/pdf-tools/issues/18#issuecomment-269515117

  (defun brds/pdf-set-last-viewed-bookmark ()
    (interactive)
    (when (eq major-mode 'pdf-view-mode)
      (bookmark-set (brds/pdf-generate-bookmark-name))))

  (defun brds/pdf-jump-last-viewed-bookmark ()
    (bookmark-set "fake")               ; this is new
    (when
        (brds/pdf-has-last-viewed-bookmark)
      (bookmark-jump (brds/pdf-generate-bookmark-name))))

  (defun brds/pdf-has-last-viewed-bookmark ()
    (assoc
     (brds/pdf-generate-bookmark-name) bookmark-alist))

  (defun brds/pdf-generate-bookmark-name ()
    (concat "PDF-LAST-VIEWED: " (buffer-file-name)))

  (defun brds/pdf-set-all-last-viewed-bookmarks ()
    (dolist (buf (buffer-list))
      (with-current-buffer buf
        (brds/pdf-set-last-viewed-bookmark))))

  (add-hook 'kill-buffer-hook 'brds/pdf-set-last-viewed-bookmark)
  (add-hook 'pdf-view-mode-hook 'brds/pdf-jump-last-viewed-bookmark)
  (unless noninteractive                ; as `save-place-mode' does
    (add-hook 'kill-emacs-hook #'brds/pdf-set-all-last-viewed-bookmarks)))

(provide 'init-pdf-tools)

;;; init-pdf-tools ends here
