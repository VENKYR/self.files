;;; package --- summary elfeed config
;;; Commentary:
;;; code:




(global-set-key (kbd "C-x w") 'elfeed)

(setq elfeed-feeds
      '(("https://feeds.feedburner.com/Torrentfreak" Pirate)
        ("http://jvns.ca/atom.xml" BL0G)
        ("https://www.cyberciti.biz/feed/" LINUX)
        ("https://planetpython.org/rss20.xml" Python)))


(provide 'init-elfeed)


;;; init-elfeed ends here
